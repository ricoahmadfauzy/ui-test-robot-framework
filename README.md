# UI Test Robot Framework #

This UI Login Test Examle for [**EBAY BLANJA**](https://ebay.blanja.com/) using [**Robot Framework**](http://robotframework.org/)


## Instalation ##

> ### For Windows ###

* ### Install Python 2.7 ###
Download and Install from this [link](https://www.python.org/ftp/python/2.7.14/python-2.7.14.amd64.msi)

	* ### Check Python Version ###
check python version on ***CMD***

		```python --version```
		
		
* ### Settings Environtment Variables ###
search on windows ‘environment variables’

![](https://res.cloudinary.com/practicaldev/image/fetch/s--c544NuKx--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/7pm3n5ndwmz5oddfn01z.png)

double click on **Path**

![](https://res.cloudinary.com/practicaldev/image/fetch/s--b2Omg9cn--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/umslp0xjckm7p62rfnh3.png)

add `C:\Python27` and `C:\Python27\Scripts` after that click 'Ok'

like this image:

![](https://res.cloudinary.com/practicaldev/image/fetch/s--AKdeVxXE--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/i1gv8f24z8qk6n8oq5oo.png)


* ### Install PIP ###
Download and Double click file from [Link](https://bootstrap.pypa.io/get-pip.py)

	* ### Check PIP Version ###
check pip version on ***CMD***

		```pip --version```
		
* ### Install Robot Framework ###
open ***CMD*** and run this script

	```pip install robotframework```

* ### Install Selenium2 Library ###
after success install robot framework, write this script

	```pip install robotframework-Selenium2Library```

* ### Install WXPython (Optional) ###
if on your system has not installed WXPython, run this script on ***CMD***

	```pip install -U wxPython``

* ### Install Chromedriver ###
download chromedriver latest version on this [link](https://chromedriver.storage.googleapis.com/2.35/chromedriver_win32.zip)

after download, extract file and copy and paste chromedriver file to `C:\Python27`

* ### Install Geckodriver ###
download chromedriver latest version on this link [32bit](https://github.com/mozilla/geckodriver/releases/download/v0.19.1/geckodriver-v0.19.1-win32.zip) or [64bit](https://github.com/mozilla/geckodriver/releases/download/v0.19.1/geckodriver-v0.19.1-win64.zip)

after download, extract file and copy and paste chromedriver file to `C:\Python27`

## Running ##

point to the place where you saved your robot project

* Robot -d (name report folder) (your test case)

	for example `Robot -d Reports test.robot`

## Documentation ##

* [Robot Framework User Guide](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html)
* [Selenium2Library](http://robotframework.org/Selenium2Library/Selenium2Library.html#Xpath%20Should%20Match%20X%20Times)
* [Builtin Library](http://robotframework.org/robotframework/latest/libraries/BuiltIn.html)
* [HowToWriteGoodTestCases](https://github.com/robotframework/HowToWriteGoodTestCases)


## Author ##
[RAF](https://github.com/ricoahmadfauzy)