*** Settings ***
Documentation       Sample Automation Login
Library             Selenium2Library
Variables           ../Variables/Variable.py

*** Keywords ***
Open Browser To Login page
    open browser   ${LOGIN_URL}   ${BROWSER}
    maximize browser window
    login should be open

Login Should Be Open
    Wait Until Page Contains    ${title}

Go To Login Page
    go to  ${LOGIN_URL}
    Login Should Be Open

Input Email
    [Arguments]  ${username}
    input text  id=nick  ${username}

Input Password
    [Arguments]  ${password}
    input text  id=password    ${password}

Submit Credentials
    click element  id=signInSubmit
#    go to  ${URL}

Welcome Page Should Be Open
#    location should be  ${URL}
    go to  ${URL}
    title should be  ${titleAfterLogin}
