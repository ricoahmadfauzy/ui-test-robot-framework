*** Settings ***
Documentation       Login with Invalid Credentials
Library             Selenium2Library
Resource            ../Keywords/KeywordLogin.robot
Variables           ../Variables/Variable.py
Suite Setup         Open Browser To Login Page
Suite Teardown      Close Browser
Test Setup          Go To Login Page
Test Template       Login With Invalid Credentials Should Fail


*** Test Cases ***          EMAIL           PASSWORD
Invalid Email               invalid         ${password}
Invalid Password            ${username}     invalid
Invalid Email and Password  invalid         whatever


*** Keywords ***
Login With Invalid Credentials Should Fail
    [Arguments]   ${username}    ${password}
    Input Email     ${username}
    input password  ${password}
    Submit Credentials
    Login Should Have Failed

Login Should Have Failed
    sleep  ${timeout}
    alert should be present   Username atau password salah, mohon coba lagi.