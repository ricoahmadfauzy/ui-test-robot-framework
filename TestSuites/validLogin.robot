*** Settings ***
Documentation       Login with valid Credentials
Library             Selenium2Library
Resource            ../Keywords/KeywordLogin.robot
Variables           ../Variables/Variable.py


*** Test Cases ***
TestCase: Login valid credentials
    [Setup]
        open browser to login page
        input email   ${username}
        input password   ${password}
        submit credentials
        welcome page should be open
    [Teardown]  close browser